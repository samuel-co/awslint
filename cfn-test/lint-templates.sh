#!/bin/bash
ERROR_COUNT=0;
TEMPLATE_PATH='.'
if [ -n $1 ]; then
  TEMPLATE_PATH=$1
fi
if [ -n $2 ]; then
  OPTIONS=$2
fi
printf "Lint CloudFormation templates..."

for FILE in $(find $TEMPLATE_PATH -regex ".*\.\(json\|y[a]*ml\|template\)"); do
  # Validate the template with cfn-lint
  ERRORS=$(cfn-lint --format quiet $OPTIONS $FILE);
  if [ "$?" -gt "0" ]; then 
    ((ERROR_COUNT++));
    printf "\n\n[fail] $FILE";
    printf "\n$ERRORS";
  else
    printf "\n\n[pass] $FILE";
  fi;
done; 

printf "\n\n$ERROR_COUNT file(s) with cfn-lint error(s)\n";
if [ "$ERROR_COUNT" -gt 0 ];
  then exit 1;
fi
